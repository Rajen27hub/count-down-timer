package com.example.myapplication2

import android.os.CountDownTimer
import androidx.lifecycle.ViewModel

class MainActivityViewModel : ViewModel() {

    var timer: CountDownTimer? = null

    fun startTimer(duration: Long) {
        timer = object : CountDownTimer(duration, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                // Do something on each tick of the timer
            }

            override fun onFinish() {
                // Do something when the timer finishes
            }
        }
        timer?.start()
    }

    fun stopTimer() {
        timer?.cancel()
        timer = null
    }
}