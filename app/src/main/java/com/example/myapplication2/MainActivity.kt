package com.example.myapplication2

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.lifecycleScope
import com.example.myapplication2.ui.theme.MyApplication2Theme
import com.softaai.livecoding.TimerDataStoreManager
import kotlinx.coroutines.launch


class MainActivity : ComponentActivity() {

    lateinit var hms: String

    lateinit var timerManager: TimerDataStoreManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        timerManager = TimerDataStoreManager(this)

        lifecycleScope.launch {
            timerManager.timer.collect { timer ->
                hms = timer
                setContent {
                    MyApplication2Theme {
                        // A surface container using the 'background' color from the theme
                        Surface(
                            modifier = Modifier.fillMaxSize(),
                            color = MaterialTheme.colors.background
                        ) {
                            CountDownTimerUI(hms)
                        }
                    }
                }
            }
        }
    }
}


@SuppressLint("UnspecifiedImmutableFlag")
@Composable
fun CountDownTimerUI(
    hms: String
) {
    Text(text = hms)

    var hh by remember {
        mutableStateOf("0")
    }
    var mm by remember {
        mutableStateOf("0")
    }
    var ss by remember {
        mutableStateOf("0")
    }
    var textFieldState by remember {
        mutableStateOf("0")
    }

    var buttonState by remember {
        mutableStateOf(true)
    }

    val context = LocalContext.current

    Column(
        modifier = Modifier.fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {

        Row(
            modifier = Modifier
                .padding(10.dp)
                .fillMaxWidth(),
            horizontalArrangement = Arrangement.Center
        ) {
            OutlinedTextField(
                modifier = Modifier
                    .width(100.dp),
                value = hh,
                onValueChange = {
                    hh = it
                },
                placeholder = {
                    Text(text = "Hour")
                },
                shape = RoundedCornerShape(24.dp),
            )
            Spacer(Modifier.width(20.dp))
            OutlinedTextField(
                modifier = Modifier
                    .width(100.dp),
                value = mm,
                onValueChange = {
                    mm = it
                },
                placeholder = {
                    Text(text = "Minutes")
                },
                shape = RoundedCornerShape(24.dp),
            )
            Spacer(Modifier.width(20.dp))
            OutlinedTextField(
                modifier = Modifier
                    .width(100.dp),
                value = ss,
                onValueChange = {
                    ss = it
                },
                placeholder = {
                    Text(text = "Seconds")
                },
                shape = RoundedCornerShape(24.dp),
            )
        }

        Button(
            onClick = {
                val tamp = hh.toInt() * 3600 + mm.toInt() * 60 + ss.toInt()
                textFieldState = tamp.toString()
                if (!textFieldState.equals("")) {
                    buttonState = false
                    val intent = Intent(Intent(context, NotificationService::class.java))
                    intent.putExtra("time", textFieldState)
                    context.startService(intent)
                } else {
                    textFieldState = "Please enter time"
                }
            },
            enabled = buttonState
        ) {
            Text("Start")
        }
    }

}

