package com.example.myapplication2

import android.annotation.SuppressLint
import android.app.*
import android.content.Context
import android.content.Intent
import android.os.*
import android.util.Log
import com.softaai.livecoding.TimerDataStoreManager
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch


class NotificationService : Service() {

    var TAG = "Timers"
    var hms = "00:00:00"


    lateinit var notificationManager: NotificationManager
    lateinit var notificationChannel: NotificationChannel
    lateinit var builder: Notification.Builder
    lateinit var notification: Notification
    private val channelId = "com.softaai.livecoding.notifications"
    private val description = "Test Notification"

    private val serviceScope = CoroutineScope(SupervisorJob())

    override fun onBind(arg0: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        Log.e(TAG, "onCreate")
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.e(TAG, "onStartCommand")
        super.onStartCommand(intent, flags, startId)
        val time = intent?.extras!!.getString("time")

        initializeTimerTask(this, time)
        createTimerNotification()
        startForeground(
            1,
            notification
        )

        return START_STICKY
    }


    override fun onDestroy() {
        Log.e(TAG, "onDestroy")
        super.onDestroy()
    }

    fun initializeTimerTask(context: Context, time: String?) {
        val timer = object : CountDownTimer(time!!.toLong() * 1000 , 1000) {
            override fun onTick(millisUntilFinished: Long) {
                val hour = millisUntilFinished / 3600000 % 24
                val min = millisUntilFinished / 60000 % 60
                val sec = millisUntilFinished / 1000 % 60

                fun formatIntTimerDisplay(t: Long): String {
                    return t.toString().padStart(2, '0')
                }
                hms = formatIntTimerDisplay(hour) + ":" + formatIntTimerDisplay(min) + ":" + formatIntTimerDisplay(sec)
                serviceScope.launch {
                    TimerDataStoreManager(context).setTimer(hms)
                }
            }

            override fun onFinish() {
                vibrate()
                raiseNotification(builder, hms)
                hms = "00:00:00"
            }
        }
        timer.start()
    }

    @SuppressLint("UnspecifiedImmutableFlag")
    private fun createTimerNotification() {
        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        val intent1 = Intent(this, MainActivity::class.java)

        val pendingIntent =
            PendingIntent.getActivity(this, 0, intent1, PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_MUTABLE)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationChannel =
                NotificationChannel(channelId, description, NotificationManager.IMPORTANCE_HIGH)
            notificationChannel.enableLights(true)
            notificationChannel.enableVibration(false)
            notificationManager.createNotificationChannel(notificationChannel)

            builder = Notification.Builder(this, channelId)
                .setContentTitle("Timer Notification")
                .setContentText(hms)
                .setOnlyAlertOnce(true)
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setContentIntent(pendingIntent)
        }

        notification = builder.build()
    }

    private fun vibrate() {
        val vibrator = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val vibrationEffect = VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE)
            vibrator.vibrate(vibrationEffect)
        } else {
            vibrator.vibrate(500)
        }
    }

    private fun raiseNotification(b: Notification.Builder, hms: String) {
        b.setContentText(hms)
        b.setOngoing(true)

        notificationManager.notify(1, b.build())
    }
}
