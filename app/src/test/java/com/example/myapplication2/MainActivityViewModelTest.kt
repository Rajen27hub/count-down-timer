package com.example.myapplication2

import junit.framework.TestCase.assertNotNull
import junit.framework.TestCase.assertNull
import org.junit.Before
import org.junit.Test

class MainActivityViewModelTest {

    private lateinit var viewModel: MainActivityViewModel

    @Before
    fun setUp() {
        viewModel = MainActivityViewModel()
    }

    @Test
    fun testStartTimer() {
        // Start the timer with a duration of 5 seconds
        viewModel.startTimer(5000)

        // Verify that the timer is not null
        assertNotNull(viewModel.timer)

        // Wait for the timer to finish
        Thread.sleep(6000)

        // Verify that the timer is null after it finishes
        assertNull(viewModel.timer)
    }

    @Test
    fun testStopTimer() {
        // Start the timer with a duration of 10 seconds
        viewModel.startTimer(10000)

        // Verify that the timer is not null
        assertNotNull(viewModel.timer)

        // Stop the timer
        viewModel.stopTimer()

        // Verify that the timer is null after it is stopped
        assertNull(viewModel.timer)
    }
}
